create database Estudio;

use Estudio;

create table Formulario(id int auto_increment primary key, numeroPreguntas int);

create table Materia(id int auto_increment primary key,nombre varchar(40),descripcion varchar(700));

create table FormularioMateria(id int auto_increment primary key, numeroFormularios int, idFormulario int, idMateria int,foreign key (idMateria) references Materia(id),foreign key(idFormulario) references Formulario(id));

create table Material(id int auto_increment primary key,datos longtext,image blob,importante varchar(3),idMateria int,foreign key (idMateria) references Materia(id));

create table Perfil(id int auto_increment primary key,promedioTotal double, nombre varchar(45));

create table Evaluacion(id int auto_increment primary key, errores int, correctas int, total int, promedio double, idFormulario int, idPerfil int,foreign key (idFormulario) references Formulario(id), foreign key (idPerfil) references Perfil(id));

create table PerfilFormulario(id int auto_increment primary key, idPerfil int, idFormulario int, foreign key (idPerfil) references Perfil(id), foreign key (idFormulario) references Formulario(id));

create table Respuesta(id int auto_increment primary key, respuesta varchar(500));

create table RespuestaCorrecta(id int auto_increment primary key, respuesta varchar(500));

create table Pregunta(id int auto_increment primary key, pregunta varchar(290), idRespuestaCorrecta int, foreign key (idRespuestaCorrecta) references RespuestaCorrecta(id));

create table PreguntaRespuesta(id int auto_increment primary key,idPregunta int,idRespuesta int, foreign key (idPregunta) references Pregunta(id), foreign key (idRespuesta) references Respuesta(id));

create table FormularioPregunta(id int auto_increment primary key,idFormulario int, idPregunta int, foreign key (idFormulario) references Formulario(id), foreign key (idPregunta) references Pregunta(id));

create table PromedioMateria(id int auto_increment primary key, idPerfil int, idMateria int, promedio int, foreign key (idPerfil) references Perfil(id), foreign key (idMateria) references Materia(id));